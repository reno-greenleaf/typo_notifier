<?php

namespace Drupal\typo_notifier\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;

class PopupController extends ControllerBase{
  /**
  * Returns a simple page.
  *
  * @return array
  *   A simple renderable array.
  */

  protected $formBuilder;
  public function __construct(FormBuilder $formBuilder) {
    $this->formBuilder = $formBuilder;
  }

  /**
  * {@inheritdoc}
  *
  * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
  *   The Drupal service container.
  *
  * @return static
  */
  public static function create(ContainerInterface $container) {
    return new static($container->get('form_builder'));
  }

  public function openForm() {
    $response = new AjaxResponse();
    $modal_form = $this->formBuilder->getForm('Drupal\typo_notifier\Form\NotifyForm');
    $response->addCommand(new OpenModalDialogCommand('Notify about Typo', $modal_form, ['width' => '800']));

    return $response;
  }
}