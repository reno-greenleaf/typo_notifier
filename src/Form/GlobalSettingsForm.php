<?php

namespace Drupal\typo_notifier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Main settings.
 */
class GlobalSettingsForm extends ConfigFormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'typo_notifier_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['typo_notifier.config',];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('typo_notifier.config');

    $form['email'] = array(
        '#type' => 'email',
        '#title' => $this->t('Email'),
        '#default_value' => $config->get('email'),
    );  

    $form['max_length'] = array(
        '#type' => 'number',
        '#title' => $this->t('Max length'),
        '#default_value' => $config->get('max_length'),
    );

    $form['popup_text'] = array(
        '#type' => 'textarea',
        '#title' => $this->t('Popup text'),
        '#default_value' => $config->get('popup_text'),
    );

    $form['limit'] = array(
        '#type' => 'number',
        '#title' => $this->t('Limit'),
        '#default_value' => $config->get('limit'),
    );

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
       $this->configFactory->getEditable('typo_notifier.config')
          ->set('email', $form_state->getValue('email'))
          ->set('max_length', $form_state->getValue('max_length'))
          ->set('popup_text', $form_state->getValue('popup_text'))
          ->set('limit', $form_state->getValue('limit'))
          ->save();

    parent::submitForm($form, $form_state);
  }
}