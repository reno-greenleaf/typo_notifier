<?php

namespace Drupal\typo_notifier\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;

class NotifyForm extends FormBase {
  /**
   * {@inheritdoc}
   */

  public function getFormId() {
    return 'typo_notifier_notify_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('typo_notifier.config');

    $form['description'] = [
      '#markup' => $config->get('popup_text'),
      '#prefix' => '<p>',
      '#suffix' => '</p>',
    ];

    $form['comment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Comment'),
      '#maxlength' => 255,
      '#size' => 10,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
  * {@inheritdoc}
  */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
  * AJAX callback handler that displays any errors or a success message.
  */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand($this->t('Success!'), $this->t('Your notification has been sent.'), ['width' => 700]));
    return $response;
  }
}