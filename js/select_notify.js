function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

function KeyPress(e) {
    var evtobj = window.event ? event : e

    if (evtobj.keyCode == 13 && evtobj.ctrlKey) {
        Drupal.ajax({url: '/typo-notifier/popup'}).execute();
    }
}

document.onkeydown = KeyPress;